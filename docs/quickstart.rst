.. _quickstart:

=================
Quick Start Guide
=================

*********************************
Get running in 5 minutes or less!
*********************************

#. Clone our Demo Application for the quickest of starts.

  * Take a look at our
    `Demo Application <https://gitlab.com/sermos/sermos-demo-client>`_
    to get up and running in under 5 minutes.

#. Sermos.yaml

  * The demo contains a ``sermos.yaml`` file, which is a configuration file that
    tells Sermos how to build your application (which API endpoints to build,
    what workers exist, etc.).

#. Contact Us

  * Contact `sermos@rho.ai <sermos@rho.ai>`_ to get your demo deployment up
    and running!

Prerequisites
#############

#. Python codebase

  * Ensure your codebase is `a valid Python package
    <https://packaging.python.org/guides/distributing-packages-using-setuptools/>`_

#. Python Environment

  * Ensure your local machine has a valid Python environment. We recommend using
    `pyenv <https://github.com/pyenv/pyenv>`_ for your python executables and
    `virtualenv <https://virtualenv.pypa.io/en/latest/>`_ to isolate your environments.
