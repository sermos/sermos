.. _index:

.. image:: _static/sermos.png

============================================
 Machine Learning Deployed in the Real-World
============================================

:Release: |version|
:Date: |today|

----

Sermos provides a set of tools and design roadmap for developers to **quickly**
and **effectively** integrate Data Science into real-world applications. The core
design decisions and recommended technologies are based on nearly a decade
of putting data science to work in demanding applications such as real-time motorsports
strategy at `Pit Rho <https://pitrho.com/>`_, custom implementations across industries
as diverse as energy, finance, and healthcare at `Rho AI <https://rho.ai/>`_, and
climate impact assesment tools at `CRANE <https://cranetool.org/>`_. With that
said, this is built by *developers and data scientists* and strives to strike an
appropriate balance between opinionated decisions and personal choice.

This is open source software and we look forward to seeing what you can build
on top of `Sermos <https://gitlab.com/sermos/sermos>`_. For those looking for quick, scalable,
enterprise-grade deployments, make sure to check out `Sermos Cloud <https://sermos.ai/>`_,
which is purpose-built for running complex, scalable, highly available Machine Learning (ML)
workloads, including Natural Language Processing (NLP), Computer Vision (CV), Decision
Modeling, Internet of Things (IoT), and more.

Sermos comprises a few key libraries:

#. `Sermos <https://gitlab.com/sermos/sermos>`_

  * This is the base package with optional scaffolding, architecture components
    (e.g. `DAG implementation <https://en.wikipedia.org/wiki/Directed_acyclic_graph>`_
    on top of
    `Celery <https://docs.celeryproject.org/en/stable/getting-started/introduction.html>`_),
    and some useful utilities.

#. `Sermos Tools <https://gitlab.com/sermos/sermos-tools>`_

  * Tool catalog for use in Sermos (or other!) Machine Learning applications.

To start using Sermos (see: :ref:`quickstart`).

For anyone using `Sermos Cloud <https://sermos.ai/>`_, you need only have:

#. A Python package with a properly configured `setup.py`
#. A simple `sermos.yaml` configuration file.
#. **Sermos handles the rest.**

========
Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Core

   quickstart
   overview
   deployments
   sermos_yaml
   cli

.. toctree::
   :maxdepth: 1
   :caption: Supporting Libraries

   Sermos Tools <https://docs.sermos.ai/sermos-tools>
   Rho ML <https://docs.rho.ai/rho-ml>


* :ref:`genindex`
* :ref:`modindex`
