.. _cli:

===========
CLI Tools
===========

Deployments
===========

.. automodule:: sermos.cli.deploy

.. automethod:: sermos.cli.deploy.validate

.. automethod:: sermos.cli.deploy.deploy

.. automethod:: sermos.cli.deploy.status

Proxy
=====

.. automodule:: sermos.cli.proxy

.. automethod:: sermos.cli.proxy.proxy
