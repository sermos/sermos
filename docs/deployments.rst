.. _deployments:

===========
Deployments
===========

Sermos provides Managed Deployments as a service through
`Sermos Cloud <https://sermos.ai/>`_. This allows your organization
to focus on the core functionality that matters to your business, we handle all
of the cloud infrastructure, scaling, availability, etc.

Prerequisites
=============

To deploy your application to Sermos, there are a few prerequisites:

#. You have created your `Deployment` environment in your
   `Sermos Cloud <https://sermos.ai/>`_ console.
#. An `access key` has been issued for that `Deployment`
#. Your app is a valid Python package with a standard structure (see below)
#. You have a valid :ref:`sermos-yaml` (`sermos.yaml`).

Deployment
==========

You can initiate a Sermos deployment in two ways: programmatically or using the
CLI tool.

It is recommended to keep your *secret* access key in the environment and
to set the client package directory in the environment as well. An example
``.env`` file would contain:

.. code-block::

    SERMOS_ACCESS_KEY=YBPiRczbyatDNVCz.zxMe8XsZTb2TxVLWTeQQ7Sa1
    SERMOS_DEPLOYMENT_ID=ec43g32d-128a-4de6-b51e-8c6a1303ec85

Programmatic Deployment
-----------------------

Invoking a pipeline programmatically (e.g. as part of a build pipeline) can
be done similar to below (assumes access key/client package directory
are available in the environment per note above).

.. code-block::

    from sermos.deploy import SermosDeploy

    sd = SermosDeploy()
    status = sd.invoke_deployment()
    print(status)

CLI Deployment
--------------

For a cli-based deployment, there is a `sermos deploy` command installed
as part of the sermos package. The example below uses a utility
[Honcho](https://pypi.org/project/honcho/) to inject the contents of the
provided environment file into the environment of the command at runtime. You
can accomplish this any other way that works for your setup.

.. code-block:: bash

    honcho run -e .env sermos deploy

Deployment Status
=================

Assuming your environment is set up per notes in the `Deployment` section above:

Programmatic Status Checks
--------------------------

.. code-block::

    from sermos.deploy import SermosDeploy

    sd = SermosDeploy()
    status = sd.get_deployment_status()
    print(status)

CLI Status Checks
-----------------

.. code-block:: bash

    honcho run -e .env sermos status

Proper Python Package Structure
===============================

Assuming your package is called "my_sermos_client":

.. code-block::

    /path/to/codebase/
        my-sermos-client/
            setup.py
            my_sermos_client/
                __init__.py
                sermos.yaml

`my_sermos_client/__init__.py` has only one requirement, to contain your
application's version assigned as a variable `__version__`, e.g.:

.. code-block::

    __version__ = '0.1.0'

Common practice is to use that value in your `setup.py` file, e.g.

.. code-block::

    _version_re = re.compile(r'__version__\s+=\s+(.*)')
    with open('my_sermos_client/__init__.py', 'rb') as f:
        __version__ = str(ast.literal_eval(_version_re.search(
            f.read().decode('utf-8')).group(1)))

Sermos Deploy
=============

.. automodule:: sermos.deploy
  :members:
