.. _sermos-yaml:

===========
Sermos Yaml
===========

.. automodule:: sermos.sermos_yaml

Yaml Format
=============

.. autoattribute:: sermos.sermos_yaml.SermosYamlSchema
