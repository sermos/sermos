""" Test Flask Sermos methods
"""
import os
import mock
from flask import Flask
from sermos import __version__


class TestFlaskSermos:
    """ Test classes in flask/flask_sermos.py
    """
    def test_flask_sermos(self):
        """ Test basic initialization
        """
        from sermos.flask import FlaskSermos
        app = Flask(__name__)
        fs = FlaskSermos()
        fs.init_app(app)
        assert isinstance(app, Flask)
