""" Test the generic utilities.
"""
from sermos.utils.module_utils import normalized_pkg_name


class TestUtils():
    def test_normalized_pkg_name(self):
        """ Verify it normalizes names appropriately
        """
        dashed_version = 'pkg-name'
        under_version = 'pkg_name'

        # Verify default behavior, which will replace any dashes with _
        normalized_dashed = normalized_pkg_name(dashed_version)
        normalized_under = normalized_pkg_name(under_version)
        assert normalized_dashed == normalized_under
        assert '-' not in normalized_dashed
        assert '_' in normalized_dashed

        # Verify 'dashed' behavior
        normalized_dashed = normalized_pkg_name(dashed_version, dashed=True)
        normalized_under = normalized_pkg_name(under_version, dashed=True)
        assert normalized_dashed == normalized_under
        assert '_' not in normalized_dashed
        assert '-' in normalized_dashed
