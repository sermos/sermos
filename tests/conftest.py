""" Test setup
"""
import os
import pytest
import boto3
import fakeredis
from flask import Flask
from moto import mock_s3
from tests.fixtures import *

S3_MODEL_BUCKET = 'sermos-client-models'


@pytest.fixture(scope="session")
def my_mock_s3(request):
    """ setUp/tearDown the moto library """
    m = mock_s3()
    m.start()

    def fin():
        m.stop()

    request.addfinalizer(fin)


@pytest.fixture(scope="session", autouse=True)
def dummy_bucket(my_mock_s3):
    """ Create an S3 bucket """
    bucket = boto3.resource('s3').Bucket(S3_MODEL_BUCKET)
    bucket.create()

    return bucket


@pytest.fixture(scope="session")
def s3_client():
    """ Return s3 client """
    client = boto3.client('s3',
                          aws_access_key_id='foo',
                          aws_secret_access_key='bar',
                          aws_session_token='baz',
                          region_name='us-east-1')
    return client


@pytest.fixture(scope="session")
def mock_redis(request):
    """ setUp/tearDown the fakeredis library """
    # Setup fake redis for testing.
    r = fakeredis.FakeStrictRedis()

    def fin():
        # Clear data in fakeredis.
        r.flushall()

    request.addfinalizer(fin)
    return r


@pytest.fixture(scope="session")
def pipeline_configs_api():
    """ Mock pipeline configurations from Sermos as they would come from
    sermos configuration API.
    """
    with open('tests/fixtures/api/demo_pipeline_conf.json', 'r') as f:
        standard_pipeline = json.loads(f.read())
    with open('tests/fixtures/api/demo_pipeline_conf_single.json', 'r') as f:
        standard_pipeline_single = json.loads(f.read())
    with open('tests/fixtures/api/demo_pipeline_conf_customer_short.json',
              'r') as f:
        customer_short_pipeline = json.loads(f.read())
    with open('tests/fixtures/api/demo_pipeline_conf_hitl.json', 'r') as f:
        hitl_pipeline = json.loads(f.read())
    with open('tests/fixtures/api/demo_pipeline_conf_invalid.json', 'r') as f:
        invalid_pipeline = json.loads(f.read())
    with open('tests/fixtures/api/demo_pipeline_conf_long.json', 'r') as f:
        long_pipeline = json.loads(f.read())
    with open('tests/fixtures/api/demo_pipeline_conf_simple.json', 'r') as f:
        simple_pipeline = json.loads(f.read())

    return {
        'standard_pipeline': standard_pipeline,
        'standard_pipeline_single': standard_pipeline_single,
        'customer_short_pipeline': customer_short_pipeline,
        'hitl_pipeline': hitl_pipeline,
        'invalid_pipeline': invalid_pipeline,
        'long_pipeline': long_pipeline,
        'simple_pipeline': simple_pipeline
    }


@pytest.fixture(scope="session")
def pipeline_configs(pipeline_configs_api):
    """ Mock pipeline configurations from Sermos, this fixture provides just the
    config and removes the rest of the API response (which is typically not
    used)
    """
    return {
        'standard_pipeline':
        pipeline_configs_api['standard_pipeline'],
        'standard_pipeline_single':
        pipeline_configs_api['standard_pipeline_single']['data']['config'],
        'customer_short_pipeline':
        pipeline_configs_api['customer_short_pipeline']['config'],
        'hitl_pipeline':
        pipeline_configs_api['hitl_pipeline']['config'],
        'invalid_pipeline':
        pipeline_configs_api['invalid_pipeline']['config'],
        'long_pipeline':
        pipeline_configs_api['long_pipeline']['config'],
        'simple_pipeline':
        pipeline_configs_api['simple_pipeline']['data']['config']
    }


@pytest.fixture(scope="session")
def app(request, mock_redis):
    """ Session-wide test `Flask` application.
    """
    flask_app = Flask(__name__)

    test_conf = (('RUN_MODE', 'test'), ('SERVER_NAME', 'localhost'))
    for conf in test_conf:
        flask_app.config.setdefault(conf[0], conf[1])

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)

    return flask_app


@pytest.fixture
def client(app):
    with app.test_client() as client:
        yield client


@pytest.fixture(scope='session')
def test_data_dir():
    return os.path.join(os.path.dirname(__file__), 'fixtures')
