#!/bin/bash -e

# AWS_ACCESS_KEY_ID - (Requried) Access Key Id
# AWS_SECRET_ACCESS_KEY - (Requried) Secret
# AWS_PROFILE_NAME - (Optional) - Corresponds to your ~/.aws/credentials [profile] value.

AWS_PROFILE_NAME=${AWS_PROFILE_NAME:=default}

mkdir /root/.aws

cat > /root/.aws/credentials <<EOL
[${AWS_PROFILE_NAME}]
aws_access_key_id=${AWS_ACCESS_KEY_ID}
aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}

EOL

echo "AWS credentials file set based on environment with profile name $AWS_PROFILE_NAME"
