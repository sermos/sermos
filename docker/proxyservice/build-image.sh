#!/bin/bash

echo "-----------------------------"
echo "Building Sermos Proxy Service"
echo "Docker image ..."
echo "-----------------------------"

# Default parameters
#
DOCKER_FILE='./docker/proxyservice/Dockerfile.tmpl'
SED=$(command -v gsed || command -v sed)

# Custom die function.
#
die() { echo >&2 -e "\nRUN ERROR: $@\n"; usage; exit 1; }

usage()
{
cat << EOF
usage: ./docker/proxyservice/build.sh [-h] [-d] [-n] [-t] [-v]

  OPTIONS:
   -h      Show this message
   -d      Dev Mode - install package from local instead of remote.
   -n      The image name.
   -t      The image tag. Required for dev mode.
   -v      Application version.
EOF
}

# Parse the command line flags.
#
while getopts "hdn:t:v:" opt; do
  case $opt in
    h)
      usage
      exit 1
      ;;
    d)
      DEV_MODE=true
      ;;
    n)
      IMAGE_NAME=${OPTARG}
      ;;
    t)
      IMAGE_TAG=${OPTARG}
      ;;
    v)
      VERSION=${OPTARG}
      ;;
    \?)
      die "Invalid option: -$OPTARG"
      ;;
  esac
done

# Recreate the build directory
rm -rf ./docker/proxyservice/build
mkdir ./docker/proxyservice/build

# Use dev Dockerfile in dev mode
if [ "$DEV_MODE" = true ]; then
  DOCKER_FILE="./docker/proxyservice/Dockerfile-dev.tmpl"
fi

# Default image name
if [ -z ${IMAGE_NAME} ]; then
    IMAGE_NAME='registry.gitlab.com/sermos/sermos/proxy'
    echo "Using default image name: $IMAGE_NAME ..."
fi

# Copy docker file ".tmpl" file to the build context as build/Dockerfile
cp $DOCKER_FILE ./docker/proxyservice/build/Dockerfile

# Add Sermos Cloud public key
cp ./bin/public_keys/sermos_proxy_1.pub ./docker/proxyservice/build/sermos_proxy_1.pub

# Add Proxy Service entrypoint
cp ./docker/proxyservice/proxy.py ./docker/proxyservice/build/proxy.py

# Read python requirements from a `requirements.txt` file in which dependencies
# are listed one per line and comments exist on their own line with "# ..."
# This function turns that file into a single string of dependencies, each
# wrapped in double quotes so it is compatible with a `pip install` command.
function prep_requirements {
  local  __resultvar=$1
  REQUIREMENTS=$($SED 's/#.*$//' $2)
  REQUIREMENTS="${REQUIREMENTS//$'\n'/ }"
  local PREPPED_REQUIREMENTS=""
  IFS=' '
  read -ra REQUIREMENTS <<< "$REQUIREMENTS"
  for i in "${REQUIREMENTS[@]}"; do # access each element of array
      PREPPED_REQUIREMENTS="${PREPPED_REQUIREMENTS} \"$i\""
  done
  eval $__resultvar="'$PREPPED_REQUIREMENTS'"
}

prep_requirements PYTHON_REQUIREMENTS requirements.txt

# Including requirements with urls in them contain `/`, which breaks the sed replacement
# so we have to escape them here...
ESCAPED_PYTHON_REQUIREMENTS=$(printf '%s\n' "$PYTHON_REQUIREMENTS" | sed -e 's/[\/&]/\\&/g')

# Replace %%PYTHON_REQUIREMENTS%% in the build/Dockerfile with actual reqs
$SED -i "s/%%PYTHON_REQUIREMENTS%%/$ESCAPED_PYTHON_REQUIREMENTS/" ./docker/proxyservice/build/Dockerfile

if [ "$DEV_MODE" = true ]; then
  echo "BUILDING IN DEV MODE ..."

  if [ -z ${IMAGE_TAG} ]; then
    die "Must provide image tag in dev mode (e.g., -t {{COMMIT_HASH}}) ...";
  fi

  make build  # Build package
  cd ./dist  # Go into dist
  PKG_FILENAME=$(ls -t sermos-*.tar.gz | head -1)  # Get latest package
  if [ -z ${PKG_FILENAME} ]; then
      die "Could not find a python package ...";
  fi
  echo "Retrieved egg filename: ${PKG_FILENAME}"
  cd ..  # Go back to project root directory and copy into build context
  cp ./dist/$PKG_FILENAME ./docker/proxyservice/build/$PKG_FILENAME

  echo "Building with tag :${IMAGE_TAG}..."
  cd ./docker/proxyservice

  # Build the Proxy image
  docker build\
    --build-arg PKG_FILENAME=$PKG_FILENAME\
    -t="${IMAGE_NAME}:${IMAGE_TAG}"\
    -t="${IMAGE_NAME}:latest"\
    build/

else
  # TODO Possibly deprecate depending on outcome of CI changes
  echo "BUILDING IN PRODUCTION MODE ..."

  if [ -z ${VERSION} ]; then
    die "Must provide application version in production mode (e.g., -v 0.1.0) ...";
  fi
  IMAGE_TAG="${VERSION}"

  echo "Building with tags :latest and :${IMAGE_TAG}..."
  cd ./docker/proxyservice

  # Build the runtime image
  docker build\
    --build-arg VERSION=$VERSION\
    -t="${IMAGE_NAME}:latest"\
    -t="${IMAGE_NAME}:${IMAGE_TAG}"\
    build/
fi

# Clean up the build directory
cd ../..
rm -rf ./docker/proxyservice/build
echo "Fin."
