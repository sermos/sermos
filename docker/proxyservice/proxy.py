""" Sermos proxy service. Job is to validate a JWT was signed by Sermos and
    open a tunnel to the specified service. This assumes that this proxy service
    is deployed in the same namespace as the target service or has another way
    of communicating with it using the provided dns record.
"""
import os
import sys
import socket
import logging
import json
from typing import Union
import jwt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

from sermos.proxy import PortForwarder, ProxyBase, PROXY_HEADER_BYTES

# Set basic logging
FORMAT = 'Sermos Cloud Proxy > %(message)s'
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(level=LOG_LEVEL, format=FORMAT)
logger = logging.getLogger(__name__)

# Standard settings able to be set through environment
HOST = os.environ.get("PROXY_HOST", "0.0.0.0")
PORT = int(os.environ.get("PROXY_PORT", 49152))
MAX_CONN = int(os.environ.get("PROXY_MAX_CONN", 15))
CLIENT_TIMEOUT = float(os.environ.get("CLIENT_TIMEOUT", 43200))
PUBLIC_KEY_PATH = '/etc/sermos_keys/sermos_proxy_1.pub'


class SermosProxyClient:
    """ An incoming connection
    """
    def __init__(self,
                 client_socket: socket.socket,
                 client_address: str,
                 debug: bool = False):
        self.client_socket = client_socket
        self.client_address = client_address
        self.debug = debug
        self.header_received = False
        self.config_received = False
        self.config_size = 0  # Included in header
        self.bytes_to_recv = PROXY_HEADER_BYTES  # Initial header size

        # The 'upstream' internal service to which we are connecting
        # e.g. clever-fox-12345
        self.target_socket = None
        self.target_host = None
        self.target_port = None

        self.valid_client = False  # To be determined by parsing header/config
        self._parse_header()
        self._parse_config()

    def decode_jwt(self, jwt_token: str) -> Union[dict, None]:
        """ Decode JWT using Sermos Public Key
        """
        # Get our public key to validate jwt
        with open(PUBLIC_KEY_PATH, "rb") as key_file:
            public_key = serialization.load_pem_public_key(
                key_file.read(), backend=default_backend())

        try:
            decoded = jwt.decode(jwt_token,
                                 key=public_key,
                                 algorithms=['RS256'])
        except jwt.exceptions.InvalidSignatureError as e:
            logger.error("[x] Invalid Signature Detected!")
            return None

        return decoded

    def _parse_header(self):
        if not self.header_received:
            try:
                data = self.client_socket.recv(self.bytes_to_recv)
                self.config_size = int.from_bytes(data, byteorder='big')
                logger.info(
                    f"[.] Expecting Config of Size: {self.config_size}\n")
                self.header_received = True
            except Exception as e:
                # TODO add actual exception handling and boot client_socket
                msg = "[x] Invalid header received ..."
                logger.error(msg)
                raise ValueError(msg)

    def _validate_target(self, target_host: str, config: dict) -> bool:
        """ Verify that the target host exists in the decoded config dictionary.

            Sermos user can use the CLI tool to select a primary/replica node
            and not use the *.sermosapp.com dns record, so we nee to search
            through whole dictionary and not just look at hostname value.

            Config looks like:

            {
                '...jwt keys...': '...irrlevant for this...',
                'k8s_metadata': {
                    'hostname': 'kind-cat-7df7e3.sermosapp.com',
                    'kind-cat-7df7e3-master': {
                        ...
                    },
                    'kind-cat-7df7e3-headless': {
                        ...
                    }
                }
            }
        """
        msg = f"[+] Validated target host: {target_host}"
        for k in config.get('k8s_metadata', {}):
            if k == 'hostname':
                if config['k8s_metadata'][k] == target_host:
                    logger.info(msg)
                    return True
            else:
                if k == target_host:
                    logger.info(msg)
                    return True
        logger.warning(f"[x] Invalid target host: {target_host}")
        return False

    def _parse_config(self):
        if not self.config_received:
            try:
                data = self.client_socket.recv(self.config_size)
                config = json.loads(data)  # JSON Payload
                self.config_received = True

                self.target_host = config['target_host']
                self.target_port = config['target_port']
                decoded_jwt = self.decode_jwt(config['jwt'])

                self._validate_target(self.target_host, decoded_jwt)

                if self.debug:
                    logger.info("[!] OVERLOADING TARGET TO LOCAL REDIS")
                    self.target_host = '127.0.0.1'
                    self.target_port = 6379
                    logger.info(f"[!] SENDING TRAFFIC TO "
                                f"{self.target_host}:{self.target_port}")
                    logger.info(f"[!] JWT: {decoded_jwt}")

            except json.decoder.JSONDecodeError:
                logger.error("[x] Invalid config received, expecting JSON ...")
                return
            except jwt.exceptions.DecodeError:
                logger.error("[x] Invalid config JWT received ...")
                return
            except Exception as e:
                logger.exception(e)
                return

            self.valid_client = True  # If we made it, gtg

    def start(self, client_socket: socket.socket):
        if not self.valid_client:
            logger.error("[x] Attempted to start with an invalid client ...")
            logger.error("[x] Exiting ...")
            return

        target_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        target_socket.connect((self.target_host, self.target_port))

        pf = PortForwarder()
        pf.remote_tunnel(target_socket, client_socket)


class SermosProxyServer(ProxyBase):
    """ The primary proxy service
    """
    def __init__(self, debug: bool = False):
        self.debug = debug

    def proxy(self):
        """ The proxy
        """
        logger.info("[+] Starting Sermos Proxy Service ...")

        proxy_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        proxy_socket.bind((HOST, PORT))
        proxy_socket.listen(MAX_CONN)

        logger.info(f"[+] Proxy Server start at: {HOST}:{PORT}")

        client_socket = None
        while True:
            logger.info("[.] Waiting for incoming connections ...")
            try:
                # Accept new incoming connection requests
                client_socket, client_address = proxy_socket.accept()
                logger.info(f"[+] Incoming connection from: {client_address}")
                spc = SermosProxyClient(client_socket, client_address,
                                        self.debug)
                if not spc.valid_client:
                    client_socket.send(b'0')  # Let client know we're closing
                    self._close_conn(client_socket)
                    continue

                client_socket.settimeout(CLIENT_TIMEOUT)  # TODO test further
                spc.start(client_socket)
                client_socket.send(b'1')  # Let client know we're gtg

            except (BrokenPipeError, ConnectionResetError):
                logger.info(f"[-] Client {client_address} disconnected ...")
                client_socket.send(b'0')
                self._close_conn(client_socket)
            except socket.gaierror:
                logger.info(f"[x] Unknown target host request. Closing ...")
                client_socket.send(b'0')
                self._close_conn(client_socket)
            except ConnectionRefusedError:
                logger.info(
                    f"[x] Refused upstream target host request. Closing ...")
                client_socket.send(b'0')
                self._close_conn(client_socket)
            except socket.timeout:
                logger.info(f"[-] Client idle too long. Closing ...")
                client_socket.send(b'0')
                self._close_conn(client_socket)
            except UnicodeDecodeError:
                logger.info(
                    "[x] Sermos Proxy Client left but still connected ...")
                # Add some timeout to kill after a while?
                logger.warning("[!] Figure out how to clean up!!")
            except Exception as e:
                msg = f"[x] Proxy Exception!\n{e}"
                logger.exception(msg)
                try:
                    self._close_conn(client_socket)
                except Exception:
                    logger.error("[x] Unable to close client socket ...")
                try:
                    self._close_conn(proxy_socket)
                except Exception:
                    logger.error("[x] Unable to close proxy socket ...")
                raise e
            except KeyboardInterrupt:
                logger.info("[-] Caught keyboard interrupt, cleaning up ...")
                if client_socket is not None:
                    try:
                        client_socket.send(b'0')
                        self._close_conn(client_socket)
                    except Exception:
                        pass
                self._close_conn(proxy_socket)
                break


if __name__ == '__main__':
    # Add anything as a single CLI argument when running this script and it
    # will start up in debug mode. That does not have guaranteed behavior but
    # has the general purpose of more easily supporting local development.
    debug_mode = False
    if len(sys.argv) > 1:
        logger.info("RUNNING IN DEBUG MODE ...")
        debug_mode = True
    sps = SermosProxyServer(debug=debug_mode)
    sps.proxy()
