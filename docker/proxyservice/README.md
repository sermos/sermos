# Sermos Proxy Service Docker Image

The Proxy Service is held within a single Docker image which has Sermos
as a dependency. This service is used primarily to gain secure access to
services (such as databases) that are held behind the private network of
a Sermos Cloud Deployment.

The Proxy Service, as currently written, only accepts credentials that are
provided by Sermos Cloud directly (in the form of a signed JWT), however,
the core concepts can be used if you would like to deploy your own Proxy
service. If you choose to use this as a basis for your work, please attribute!

## Usage

In your `Sermos Cloud` dashboard, you can create a new `Proxy Service` inside
any of your `Deployments`. When that is deployed you will need to:

* Copy the `Service ID` of the Proxy Service
* Copy the `Service ID` of the Target Service to which you want to connect
(e.g. a redis database)
* Ensure you're in a python environment that has the latest Sermos library
installed with it's CLI tools.
* Run the Proxy CLI command! e.g.:

    $  sermos proxy --service-id high-ocelot-46055a --local-port 65432 --proxy-id vast-mammoth-a1a424

* With this, you can connect to your remote database on your localhost port
`65432` in the example above. e.g. `redis-cli -h 127.0.0.1 -p 65432 -a {{pwd}}`

## Build Image

No CI/CD pipeline currently exists as this service is intended not to change
much in the near term (so isn't worth the extra effort). To build a new
version using the current local version of Sermos, simply run:

    $ ./docker/proxyservice/build-image.sh -d -t {{ your tag, e.g. dev1 }}

To build using a valid existing version of Sermos, you can run in production
mode with:

    $ ./docker/proxyservice/build-image.sh -v 0.84.1
